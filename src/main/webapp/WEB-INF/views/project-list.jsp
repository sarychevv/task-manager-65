<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT LIST</h1>
<table width="100%" border="1" style="border-collapse: collapse">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" align="left" nowrap="nowrap">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="150" align="center" nowrap="nowrap">STATUS</th>
        <th width="100" align="center" nowrap="nowrap">START</th>
        <th width="100" align="center" nowrap="nowrap">FINISH</th>
        <th width="100" align="center" nowrap="nowrap">EDIT</th>
        <th width="100" align="center" nowrap="nowrap">DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr align="center">
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status}"/>
            </td>
            <td align="center">
                <f:formatDate pattern="dd.MM.yyyy" value="${project.dateStart}"/>
            </td>
            <td align="center">
                <f:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}"/>
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/project/create" style="margin-top: 20px">
    <button>CREATE PROJECT</button>
</form>
<jsp:include page="../include/_footer.jsp"/>

