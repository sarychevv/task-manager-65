package ru.vsarychev.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.vsarychev.tm")
public class ApplicationConfiguration {
}
