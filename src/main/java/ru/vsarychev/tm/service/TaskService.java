package ru.vsarychev.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vsarychev.tm.api.repository.ITaskRepository;
import ru.vsarychev.tm.api.service.ITaskService;
import ru.vsarychev.tm.exception.IdEmptyException;
import ru.vsarychev.tm.exception.TaskEmptyException;
import ru.vsarychev.tm.exception.TasksEmptyException;
import ru.vsarychev.tm.model.Task;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @Nullable
    public Task findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void create(@Nullable Task task) {
        if (task == null) throw new TaskEmptyException();
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void update(@Nullable Task task) {
        if (task == null) throw new TaskEmptyException();
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void deleteById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskRepository.deleteById(id);
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public void saveAll(@Nullable List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TasksEmptyException();
        taskRepository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }
}
