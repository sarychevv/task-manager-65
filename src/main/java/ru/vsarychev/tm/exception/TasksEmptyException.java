package ru.vsarychev.tm.exception;

public final class TasksEmptyException extends AbstractException {

    public TasksEmptyException() {
        super("Error! Tasks is empty...");
    }

}
