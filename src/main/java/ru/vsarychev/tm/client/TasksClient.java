package ru.vsarychev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Task;

import java.util.List;

public interface TasksClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/tasks";

    static TasksClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TasksClient.class, BASE_URL);
    }

    @Nullable
    @GetMapping()
    List<Task> get();

    @PostMapping
    void post(@RequestBody @Nullable List<Task> tasks);

    @PutMapping
    void put(@RequestBody @Nullable List<Task> tasks);

    @DeleteMapping()
    void delete();

}
