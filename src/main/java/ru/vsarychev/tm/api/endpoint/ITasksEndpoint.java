package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Task;

import java.util.List;

public interface ITasksEndpoint {

    @Nullable
    @GetMapping()
    List<Task> get();

    @PostMapping
    void post(@RequestBody @Nullable List<Task> tasks);

    @PutMapping
    void put(@RequestBody @Nullable List<Task> tasks);

    @DeleteMapping()
    void delete();
}
