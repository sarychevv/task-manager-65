package ru.vsarychev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.vsarychev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @Nullable
    Task findById(@Nullable String id);

    @Transactional
    void create(@Nullable Task task);

    @Transactional
    void update(@Nullable Task task);

    @Transactional
    void deleteById(@Nullable String id);

    @Nullable
    @Transactional
    List<Task> findAll();

    @Transactional
    void saveAll(@Nullable List<Task> tasks);

    @Transactional
    void removeAll();
}
