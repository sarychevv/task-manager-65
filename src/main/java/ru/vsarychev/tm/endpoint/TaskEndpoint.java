package ru.vsarychev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.api.endpoint.ITaskEndpoint;
import ru.vsarychev.tm.api.service.ITaskService;
import ru.vsarychev.tm.model.Task;

@RestController
@RequestMapping("/api/task")
public final class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @Nullable
    @GetMapping("/{id}")
    public Task get(@PathVariable("id") @Nullable String id) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping
    public void post(@RequestBody @Nullable Task task) {
        taskService.create(task);
    }

    @Override
    @PutMapping
    public void put(@RequestBody @Nullable Task task) {
        taskService.update(task);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @Nullable String id) {
        taskService.deleteById(id);
    }

}