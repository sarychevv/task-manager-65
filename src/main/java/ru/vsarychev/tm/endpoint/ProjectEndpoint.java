package ru.vsarychev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.api.endpoint.IProjectEndpoint;
import ru.vsarychev.tm.api.service.IProjectService;
import ru.vsarychev.tm.model.Project;

@RestController
@RequestMapping("/api/project")
public final class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    @GetMapping("/{id}")
    public Project get(@PathVariable("id") @Nullable String id) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping
    public void post(@RequestBody @Nullable Project project) {
        projectService.create(project);
    }

    @Override
    @PutMapping
    public void put(@RequestBody @Nullable Project project) {
        projectService.update(project);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @Nullable String id) {
        projectService.deleteById(id);
    }

}