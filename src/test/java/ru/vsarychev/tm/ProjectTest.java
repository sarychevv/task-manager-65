package ru.vsarychev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vsarychev.tm.client.ProjectClient;
import ru.vsarychev.tm.marker.IntegrationCategory;
import ru.vsarychev.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

@Category(IntegrationCategory.class)
public class ProjectTest {

    private static final Integer SIZE = (new Random()).nextInt(10 - 1 + 1) + 1;
    @Nullable
    private static List<Project> projectList;
    @NotNull
    private final ProjectClient projectClient = ProjectClient.client();

    @Before
    public void before() {
        projectList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            @NotNull Project project = new Project();
            project.setName("Project" + i);
            project.setDescription("description" + i);
            projectClient.put(project);
            projectList.add(project);
        }
    }

    @After
    public void after() {
        for (int i = 0; i < projectList.size(); i++) {
            projectClient.delete(projectList.get(i).getId());
        }
    }

    @Test
    public void get() {
        final Project project = projectClient.get(projectList.get(0).getId());
        assertNotNull(project);
    }

    @Test
    public void post() {
        @NotNull Project project = new Project();
        project.setName("ProjectCreated");
        project.setDescription("DescriptionCreated");
        projectClient.post(project);
        projectList.add(project);
        assertEquals(projectClient.get(SIZE.toString()).getName(), "ProjectCreated");
        assertEquals(projectClient.get(SIZE.toString()).getDescription(), "DescriptionCreated");
        after();
        before();
    }

    @Test
    public void put() {
        final Project project = projectClient.get(projectList.get(0).getId());
        project.setDescription("UpdDesc");
        project.setName("UpdName");
        projectClient.put(project);
        assertEquals(projectClient.get(projectList.get(0).getId()).getName(), "UpdName");
        assertEquals(projectClient.get(projectList.get(0).getId()).getDescription(), "UpdDesc");
    }

    @Test
    public void delete() {
        for (int i = 0; i < projectList.size(); i++) {
            projectClient.delete(projectList.get(i).getId());
        }
        assertNull(projectClient.get(projectList.get(0).getId()));
        before();
    }

}
